var io = require('socket.io')();
io.on('connection', function(socket){});
io.listen(3030);

var PeerServer = require('peer').PeerServer;
var peerServer = PeerServer({port: 9000});

peerServer.on('connection', function (id) {
  io.emit('user_connected', id);
  console.log('User connected with #', id);
});

peerServer.on('disconnect', function (id) {
  // io.emit(Topics.USER_DISCONNECTED, id);
  console.log('User disconnected with #', id);
});
