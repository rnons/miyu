angular.module('miyu.services')

.factory('RTC', function() {
  return {
    peer: null,
    start: function(id) {
      this.peer = new Peer(id, {
        host: 'localhost',
        port: 9000
      });
      return this.peer;
    }
  }
})

.factory('Dispatcher', function($rootScope, $state, CurrentUser, Contacts, Socket, RTC) {
  var peer, conn;

  Socket.connect('http://localhost:3030', {transports: ['websocket']});

  Socket.on('user_connected', function(data) {
    if (CurrentUser.id !== data && peer) {
      conn = peer.connect(data);
      registerRTCHandler(conn);
    }
  });

  function connectRTC() {
    peer = RTC.start(CurrentUser.id);
    peer.on('connection', function(conn) {
      registerRTCHandler(conn);
    });
    peer.on('call', function(call) {
      console.log('called from', call.peer);
      Contacts.get(call.peer).call = call
      $state.go('call', {id: call.peer, called: true});
    });
  }

  function registerRTCHandler(conn) {
    var contact = Contacts.add(conn.peer, conn);
    conn.on('open', function() {
      // Receive messages
      conn.on('data', function(data) {
        contact.inbox.push({
          from: conn.peer,
          text: data
        });
        $rootScope.$applyAsync();
      });

      $rootScope.$applyAsync();
    });
  }

  return {
    connectRTC: connectRTC,
  }
})
