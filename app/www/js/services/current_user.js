angular.module('miyu.services', [])

.factory('CurrentUser', function() {
  var id, currentUser;

  function init(id) {
    this.id = id;
    window.localStorage.setItem('id', id);
  }

  currentUser = {
    init: init
  }

  id = window.localStorage.getItem('id');
  if (id) {
    currentUser.init(id);
  }

  return currentUser;
})
