angular.module('miyu.services')

.factory('Messages', function() {
  _messages = {}

  function init(id) {
    _messages[id] = []
  }

  function get(id) {
    return _messages[id]
  }

  function add(id, text) {
    _messages[id].push({
      from: id,
      text: text
    });
  }

  return {
    init: init,
    get: get,
    add: add
  }
});
