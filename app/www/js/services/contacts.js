angular.module('miyu.services')

.factory('Contacts', function() {
  _contacts = {}

  function add(id, conn) {
    _contacts[id] = {
      id: id,
      conn: conn,
      inbox: []
    }
    return _contacts[id];
  }

  function get(id) {
    return _contacts[id];
  }
  
  function all() {
    return Object.keys(_contacts);
  }

  return {
    add: add,
    get: get,
    all: all
  }
})
