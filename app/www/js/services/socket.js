angular.module('miyu.services')

.factory('Socket', function() {
  var _socket;

  function connect(url, opts) {
    _socket = io(url, opts);
  }

  function on(eventName, handler) {
    _socket.on(eventName, handler)
  }

  return {
    connect: connect,
    on: on
  }
})
