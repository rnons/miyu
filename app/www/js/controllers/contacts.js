angular.module('miyu.controllers', [])

.controller('ContactsCtrl', function($scope, CurrentUser, Contacts, Dispatcher) {
  $scope.model = {}
  $scope.model.nick = CurrentUser.id;

  if (CurrentUser.id) {
    Dispatcher.connectRTC();
  }

  $scope.start = function() {
    $scope.model.nick = $scope.model.newNick;
    CurrentUser.init($scope.model.newNick);
    Dispatcher.connectRTC();
  }

  $scope.getContacts = function() {
    return Contacts.all();
  }
})
