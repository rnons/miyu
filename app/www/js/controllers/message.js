angular.module('miyu.controllers')

.controller('MessageCtrl', function($scope, $stateParams, CurrentUser, Contacts, Dispatcher) {
  $scope.model = {}
  $scope.model.id = $stateParams.id;
  var contact = Contacts.get($scope.model.id);
  if (!contact) {
    document.location = '/';
  }

  $scope.messages = contact.inbox;

  $scope.send = function() {
    contact.conn.send($scope.model.newMsg);
    contact.inbox.push({
      from: CurrentUser.id,
      text: $scope.model.newMsg
    });
    $scope.model.newMsg = '';
  }
})
