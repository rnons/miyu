angular.module('miyu.controllers')

.controller('CallCtrl', function($scope, $stateParams, $ionicHistory, Contacts, RTC) {
  $scope.model = {}
  $scope.model.id = $stateParams.id;
  var contact = Contacts.get($scope.model.id);
  if (!contact) {
    document.location = '/';
  }

  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
  navigator.getUserMedia({video: true, audio: false}, function(stream) {
    var call;
    document.getElementById('local').src = URL.createObjectURL(stream);
    if (contact.call) {
      call = contact.call;
      call.answer(stream);
    } else {
      call = RTC.peer.call($scope.model.id, stream);
    }
    call.on('stream', function(remoteStream) {
      document.getElementById('remote').src = URL.createObjectURL(remoteStream);
    });

    call.on('close', function() {
      if (!stream.ended) {
        $ionicHistory.goBack()
      }
    });

    $scope.$on('$destroy', function() {
      stream.stop();
      call.close();
      contact.call = null;
    })
  }, function(err) {
    console.log('Failed to get local stream' ,err);
  });
})
